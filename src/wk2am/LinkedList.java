package wk2am;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LinkedList<E> implements List<E> {

    private Node head;

    private class Node {
        private E value;
        private Node next;

        public Node(E value) {
            this(value, null);
        }

        public Node(E value, Node next) {
            this.value = value;
            this.next = next;
        }
    }

    public LinkedList() {
        head = null;
    }

    @Override
    public boolean isEmpty() {
        return head==null;
    }

    @Override
    public void clear() {
        head = null;
    }

    @Override
    public int size() {
        int count = 0;
        Node walker = head;
        while(walker!=null) {
            ++count;
            walker = walker.next;
        }
        return count;
    }

    @Override
    public boolean add(E element) {
        if(isEmpty()) {
            head = new Node(element);
        } else {
            Node walker = head;
            while(walker.next!=null) {
                walker = walker.next;
            }
            walker.next = new Node(element);
        }
        return true;
    }

    @Override
    public boolean remove(Object target) {
        return false;
    }

    @Override
    public E get(int index) {
        Node walker = walkerTo(index);
        return walker.value;
    }

    @Override
    public E set(int index, E element) {
        Node walker = walkerTo(index);
        E oldValue = walker.value;
        walker.value = element;
        return oldValue;
    }

    @Override
    public void add(int index, E element) {
        if(index==0) {
            head = new Node(element, head);
        } else {
            Node walker = walkerTo(index-1);
            walker.next = new Node(element, walker.next);
        }
    }

    @Override
    public E remove(int index) {
        if(head==null) {
            throw new IndexOutOfBoundsException("Index: " + index + " Size: 0");
        }
        Node current = null;
        if(index==0) {
            current = head;
            head = head.next;
        } else {
            Node previous = walkerTo(index - 1);
            current = previous.next;
            Node next = current.next;
            previous.next = next;
        }
        return current.value;
    }

    @Override
    public int indexOf(Object target) {
        return 0;
    }

    @Override
    public boolean contains(Object target) {
        Iterator<E> itr = iterator();
        boolean found = false;
        while(!found && itr.hasNext()) {
            E element = itr.next();
            found = target==null ? element==null : target.equals(element);
        }
        return found;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    private Node walkerTo(int index) {
        if(index<0) {
            throw new IndexOutOfBoundsException("Index cannot be negative, index=" + index);
        }
        Node walker = head;
        for(int i=0; i<index; ++i) {
            if(walker==null) {
                throw new IndexOutOfBoundsException("Index: " + index + " Size: " + i);
            }
            walker = walker.next;
        }
        return walker;
    }

    @Override
    public Iterator<E> iterator() {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }
}
