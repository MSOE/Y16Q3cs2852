package wk7am;

import wk6am.BinarySearch;

import java.util.*;

public class BinarySearchTree<E extends Comparable<E>> implements Set<E> {
    public static void main(String[] args) {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        tree.add(18);
        tree.add(72);
        tree.add(19);
        tree.add(20);
        tree.add(21);
        tree.add(22);
        tree.add(23);
        BinarySearchTree<Integer> tree2 = new BinarySearchTree<>();
        tree2.add(21);
        tree2.add(19);
        tree2.add(18);
        tree2.add(20);
        tree2.add(23);
        tree2.add(72);
        tree2.add(22);
        System.out.println(tree.height());
        System.out.println(tree2.height());
    }

    private class Node {
        private E value;
        private Node lKid;
        private Node rKid;
        private Node parent;

        private Node(E value, Node lKid, Node rKid) {
            this.value = value;
            this.lKid = lKid;
            this.rKid = rKid;
            this.parent = null;
        }
    }

    private Node root;

    public BinarySearchTree() {
        root = null;
    }

    @Override
    public int size() {
        return size(root);
    }

    private int size(Node subroot) {
        return subroot==null ? 0 : 1 + size(subroot.lKid) + size(subroot.rKid);
    }

    public int height() {
        return height(root);
    }

    public void inOrder() {
        inOrder(root);
    }

    private void inOrder(Node subroot) {
        if(subroot!=null) {
            inOrder(subroot.lKid);
            System.out.println(subroot.value);
            inOrder(subroot.rKid);
        }
    }

    private int height(Node subroot) {
        return subroot==null ? 0 : 1 + Math.max(height(subroot.rKid), height(subroot.lKid));
    }

    private Node rightRotate(Node z) {
        if(z==null || z.rKid==null) {
            throw new IllegalArgumentException("Node must exist and have a right child in order to right rotate.");
        }
        Node y = z.lKid;
        Node C = y.rKid;
        Node p = z.parent;
        z.lKid = C;           // 1
        y.parent = z.parent;  // 2
        y.rKid = z;           // 3
        if(C!=null) {
            C.parent = z;     // 4
        }
        z.parent = y;         // 5
        if(p==null) {
            root = y;         // 6
        } else if(z==p.lKid) {
            p.lKid = y;       // 6
        } else {
            p.rKid = y;       // 6
        }
        return y;
    }

    @Override
    public boolean isEmpty() {
        return root==null;
    }

    @Override
    public boolean contains(Object target) {
        boolean found = false;
        try {
            found = contains(root, (E)target);
        } catch (ClassCastException e) {
            // Definitely not equal
        }
        return found;
    }

    private boolean contains(Node subroot, E target) {
        boolean found = false;
        if(subroot!=null) {
            if(subroot.value.equals(target)) {
                found = true;
            } else if(subroot.value.compareTo(target)<0) {
                found = contains(subroot.rKid, target);
            } else {
                found = contains(subroot.lKid, target);
            }
        }
        return found;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(E element) {
        if(element==null) {
            throw new NullPointerException("Null elements not supported");
        }
        boolean added = false;
        if(root==null) {
            root = new Node(element, null, null);
            added = true;
        } else {
            added = add(root, element);
        }
        return added;
    }

    private boolean add(Node subroot, E element) {
        boolean added = false;
        if(!element.equals(subroot.value)) {
            if(subroot.value.compareTo(element)>0) {
                if(subroot.lKid==null) {
                    subroot.lKid = new Node(element, null, null);
                    subroot.lKid.parent = subroot;
                    added = true;
                } else {
                    added = add(subroot.lKid, element);
                }
            } else {
                if(subroot.rKid==null) {
                    subroot.rKid = new Node(element, null, null);
                    subroot.rKid.parent = subroot;
                    added = true;
                } else {
                    added = add(subroot.rKid, element);
                }
            }
        }
        return added;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

}










