package wk6pm;

public class BinarySearch {
    public static void main(String[] args) {
        int[] data = {2, 4, 6, 8, 10, 12, 14};
        for(int i=1; i<15; ++i) {
            System.out.println("Searching for: " + i + " found: " + binarySearch(data, i));
        }
//        System.out.println(binarySearch(data, 3));
    }

    private static boolean binarySearch(int[] data, int target) {
        return binarySearch(data, target, 0, data.length);
    }

    private static boolean binarySearch(int[] data, int target, int start, int end) {
        boolean found = false;
        if(start!=end) {
            int midpoint = (start+end)/2;
            if(target==data[midpoint]) {
                found = true;
            } else if(target<data[midpoint]) {
                found = binarySearch(data, target, start, midpoint);
            } else {
                found = binarySearch(data, target, midpoint+1, end);
            }
        }
        return found;
    }

    private static boolean binarySearch4(int[] data, int target, int start, int end) {
        int numElementsBetweenStartAndEnd = end - start;
        int[] newData = new int [numElementsBetweenStartAndEnd];
        int index = 0;

        for (int i = start; i < end; i++) {
            newData[index] = data[i];
            index++;
        }
        int indexOfMiddleElement;
        if (newData.length % 2 == 0) {
            indexOfMiddleElement = numElementsBetweenStartAndEnd / 2 - 1;
        } else {
            indexOfMiddleElement = numElementsBetweenStartAndEnd / 2;
        }
        if (target == newData[indexOfMiddleElement]) {
            return true;
        } else if (target < newData[indexOfMiddleElement]) {
            return newData.length != 1 && binarySearch4(newData, target, 0, indexOfMiddleElement);
        } else {
            return newData.length != 1 && binarySearch4(newData, target, indexOfMiddleElement + 1, newData.length);
        }
    }

    private static boolean binarySearch5(int[] data, int target, int start, int end) {
        if(start==end) {
            return false;
        }
        int mid = (start+end)/2;
        if(target==data[mid]){
            return true;
        } else if(target>data[mid]){
            return binarySearch5(data,target,mid+1,end);
        } else {
            return binarySearch5(data,target,start,mid);
        }
    }

/*
    private static boolean binarySearch6(int[] data, int target, int start, int end) {
        if (data[end/2] < target){
            binarySearch6(data, target, 0, data/2);
        }else if (data[end/2] > target){
            binarySearch6(data, target, data/2, end);
        } else if (data[end/2] == target){
            return true;
        }
    }

    private static boolean binarySearch7(int[] data, int target, int start, int end) {
        boolean answer = false;

        if(start != end){
            if(data[(end + start)/2] > target) {
                binarySearch7(data, target, (end + start)/2, end);
            } else if (data[(end + start)/2] < target) {
                binarySearch7(data, target, start, (end + start)/2);
            } else if (data[(end + start)/2] == target) {
                answer = true;
            }
        }
        return answer;
    }

    private static boolean binarySearch8(int[] data, int target, int start, int end) {
        boolean isFound = false;
        int middle = (start+end)/2;
        int value = data[middle];
        if(start!=end) {
            if(value==target) {
                isFound = true;
            } else if(target>value) {
                isFound = binarySearch8(data, target, middle+1, end);
            } else {
                isFound = binarySearch8(data, target, start, middle);
            }
        }
        return isFound;
    }

    private static boolean binarySearch9(int[] data, int target, int start, int end) {
        boolean returning = false;
        if (end == start) {
            return returning;
        }
        if (target > data[(start + (int)(Math.floor((end - start) / 2.0)))]) {
            start = start + (int)Math.ceil(((end - start) / 2.0));
        } else if (target < data[end - (int)Math.ceil(((end - start) / 2.0))]) {
            end = end - (int)Math.ceil(((end - start) / 2.0));
        } else {
            returning = true;
        }

        return returning || binarySearch9(data, target, start, end);
    }
*/
/*
    private static boolean binarySearch1(int[] data, int target, int start, int end) {
        int middleIndex = (end-start)/2;
        boolean isFound = false;
        if (start==end) {
            isFound = false;
        }else if(target==data[middleIndex]){
            isFound = true;
        }else if(target<data[middleIndex]){
            isFound = binarySearch1(data, target, start, middleIndex-1);
        }else if(target>data[middleIndex]){
            isFound = binarySearch1(data, target, middleIndex+1, end);
        }
        return isFound;
    }

    private static boolean binarySearch2(int[] data, int target, int start, int end) {
        int middleIndex = start + (end / 2);
        if (target == data[middleIndex]) {
            return true;
        }

        int nextStart = (target < data[middleIndex]) ? start : middleIndex;
        int nextEnd = (target < data[middleIndex]) ? middleIndex : end;

        if (nextStart == nextEnd) {
            return false;
        }

        return (binarySearch2(data, target, nextStart, nextEnd));
    }

    private static boolean binarySearch3(int[] data, int target, int start, int end) {
        int index = (top + bottom) / 2;
        if((top - bottom) <= 1 && numbers[index] != target){
            return -1;
        }
        if(numbers[index] == target){
            return index;
        }
        else if (numbers[index] > index){
            return binarySearch3(target, 0, index, numbers);
        }
        else if (numbers[index] < index){
            return binarySearch3(target, index, numbers.length, numbers);
        }
        else{
            return -1;
        }
    }
*/
}
