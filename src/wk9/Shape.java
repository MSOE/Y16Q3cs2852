package wk9;

import java.awt.Color;
import java.awt.Point;

public class Shape {
    private Color color;
    private Point center;

    public Shape(int x, int y, Color color) {
        this(new Point(x, y), color);
    }

    protected Shape(Point center, Color color) {
        this.center = center;
        this.color = color;
    }

    public Shape(Shape shape, boolean isDeep) {
        if(!isDeep) {
            this.center = shape.center;
            this.color = shape.color;
        } else {
            this.center = new Point(shape.center);
            this.color = new Color(shape.color.getRGB());
        }
    }

    public void setX(int x) {
        center.setLocation(x, center.getY());
    }

    public void setY(int y) {
        center.setLocation(center.getX(), y);
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Shape at (" + center.getX() + ", " + center.getY()
                + ") with color: (" + color.getRed() + ", "
                + color.getGreen() + ", " + color.getBlue() + ")";
    }
}
