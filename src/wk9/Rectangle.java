package wk9;

import java.awt.Dimension;
import java.awt.Color;

public class Rectangle extends Shape {
    protected Dimension size;

    public Rectangle(int x, int y, Color color, int width, int height) {
        super(x, y, color);
        size = new Dimension(width, height);
    }

    public Rectangle(Rectangle rect) {
        super(rect, true);
        size = new Dimension(rect.size);
    }

    @Override
    public String toString() {
        String result = super.toString();
        return "(" + size.getWidth() + ", " + size.getHeight() + ") Rectangle"
                + result.substring(5);
    }

    public void setSize(int width, int height) {
        size.setSize(width, height);
    }
}
