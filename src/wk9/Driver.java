package wk9;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class Driver {
    public static void main(String[] args) {
        Shape shape1 = new Shape(1, 8, Color.GREEN);
        Shape shape2 = shape1;
        Shape shape3 = new Shape(shape1, true);
        Shape shape4 = new Rectangle(0, 0, Color.MAGENTA, 27, 28);
        Shape shape5;
        if(shape4 instanceof Rectangle) {
            shape5 = new Rectangle((Rectangle)shape4);
        } else {
            shape5 = new Shape(shape4, true);
        }
        shape1.setY(0);
        System.out.println(shape1);
        System.out.println(shape3);
    }





















}
