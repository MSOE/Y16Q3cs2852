package wk1;

import javax.swing.JOptionPane;
import java.util.Iterator;
import java.util.List;

public class Driver {
    public static void main(String[] args) {
        List<String> words = new java.util.ArrayList<>();
        words.add("first");
        words.add("second");
        words.add("third");

        {
            Iterator<String> itr = words.iterator();
            itr.next();
            itr.remove();
            while(itr.hasNext()) {
                String stuff = itr.next();
                System.out.println(stuff);
            }
        }

        for(String stuff : words) {
            System.out.println(stuff);
        }
    }
}
