package wk1;

import java.util.*;

public class ArrayList<E> implements List<E> {
    public static void main(String[] args) {
        List<Double> numbers = new ArrayList<>();
        for(int i=0; i<10; i++) {
            numbers.add(i+0.5);
        }
        System.out.println(numbers.toString());
    }

    private E[] data;

    public ArrayList() {
        data = (E[])new Object[0];
    }

    // Write a recursive toString method for our LinkedList
    // private String toString(Node start);
    //
    // private boolean binarySearch(int[] data, int target, int start, int end);
    @Override
    public String toString() {
        String answer = toString(0);
        return "[" + answer.substring(0, answer.length()-2) + "]";
    }

    // [3, 5, 7, 9, 13, 2, 1]
    private String toString(int start) {
        String answer = "";
        if(start<size()) {
            answer = "" + get(start) + ", " + toString(start+1);
        }
        return answer;
    }

    @Override
    public int size() {
        return data.length;
    }

    @Override
    public boolean isEmpty() {
        return size()==0;
    }

    @Override
    public boolean add(E element) {
        E[] newGuy = (E[])new Object[size()+1];
        for(int i=0; i<size(); ++i) {
            newGuy[i] = data[i];
        }
        newGuy[newGuy.length-1] = element;
        data = newGuy;
        return true;
    }

    @Override
    public boolean contains(Object target) {
        return indexOf(target)!=-1;
    }

    @Override
    public boolean remove(Object target) {
        int index = indexOf(target);
        boolean found = index!=-1;
        if(found) {
            remove(index);
        }
        return found;
    }

    @Override
    public void clear() {
        data = (E[])new Object[0];
    }

    @Override
    public E get(int index) {
        rangeCheck(index);
        return data[index];
    }

    private void rangeCheck(int index) {
        if(index<0 || index>=size()) {
            throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
        }
    }

    @Override
    public E set(int index, E element) {
        E removedElement = get(index);
        data[index] = element;
        return removedElement;
    }

    @Override
    public void add(int index, E element) {
        rangeCheck(index);
        E[] newGuy = (E[])new Object[size()+1];
        for(int i=0; i<index; ++i) {
            newGuy[i] = data[i];
        }
        newGuy[index] = element;
        for(int i=index; i<size(); ++i) {
            newGuy[i+1] = data[i];
        }
        data = newGuy;
    }

    @Override
    public E remove(int index) {
        E removedElement = get(index);
        E[] newGuy = (E[])new Object[size()-1];
        for(int i=0; i<index; ++i) {
            newGuy[i] = data[i];
        }
        for(int i=index; i<size()-1; ++i) {
            newGuy[i] = data[i+1];
        }
        data = newGuy;
        return removedElement;
    }

    @Override
    public int indexOf(Object target) {
        int index = 0;
        while(index<size() && target!=get(index)
                && (target==null || !target.equals(get(index)))) {
        //while(index<size() &&
        //        (target==null ? get(index)!=null : !target.equals(get(index)))) {
            ++index;
        }
        return index==size() ? -1 : index;
    }

    @Override
    public Object[] toArray() {
        return data;
    }

    private class ArrayListIterator implements Iterator<E> {

        private int position = -1;
        private boolean canRemove = false;

        @Override
        public boolean hasNext() {
            return position+1<size();
        }

        @Override
        public E next() {
            if(!hasNext()) {
                throw new NoSuchElementException("No elements left to visit");
            }
            canRemove = true;
            return data[++position];
        }

        @Override
        public void remove() {
            if(!canRemove) {
                throw new IllegalStateException("Must call next() first");
            }
            canRemove = false;
            ArrayList.this.remove(position);
        }
    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayListIterator();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }
}
