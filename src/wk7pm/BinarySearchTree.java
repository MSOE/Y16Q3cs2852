package wk7pm;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class BinarySearchTree<E extends Comparable<E>> implements Set<E> {
    public static void main(String[] args) {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        System.out.println(tree);
        System.out.println(tree.add(6));
        System.out.println(tree.add(7));
        System.out.println(tree.add(66));
        System.out.println(tree.add(7));
        System.out.println(tree.add(77));
        System.out.println(tree.add(32));
        System.out.println(tree.add(42));
        System.out.println(tree.add(8));
        System.out.println(tree);
        System.out.println(tree.size());
        System.out.println(tree.height());
    }
    private static class Node<E> {
        private E value;
        private Node<E> lKid;
        private Node<E> rKid;
        private Node(E value, Node<E> lKid, Node<E> rKid) {
            this.value = value;
            this.lKid = lKid;
            this.rKid = rKid;
        }
        private Node(E value) {
            this(value, null, null);
        }
    }

    @Override
    public String toString() {
        String result = toString(root);
        return "[" + (result.length()>2 ? result.substring(0, result.length()-2) : "") + "]";
    }

    private String toString(Node<E> subroot) {
        String result = "";
        if(subroot!=null) {
            result = toString(subroot.lKid) + subroot.value + ", "
                    + toString(subroot.rKid);
        }
        return result;
    }

    private Node root;

    public BinarySearchTree() {
        root = null;
    }

    @Override
    public int size() {
        return size(root);
    }

    private int size(Node<E> subroot) {
        return subroot==null ? 0 : size(subroot.lKid) + size(subroot.rKid) + 1;
    }

    @Override
    public boolean isEmpty() {
        return root==null;
    }

    public int height() {
        return height(root);
    }

    private int height(Node<E> subroot) {
        return subroot==null ? 0 : 1 + Math.max(height(subroot.lKid), height(subroot.rKid));
    }

    @Override
    public boolean contains(Object target) {
        boolean found = false;
        try {
            found = contains(root, (E) target);
        } catch (ClassCastException e) {
            // nothing to do
        }
        return found;
    }

    private boolean contains(Node<E> subroot, E target) {
        boolean found = false;
        if(subroot!=null) {
            if(subroot.value.equals(target)) {
                found = true;
            } else if(subroot.value.compareTo(target)<0) {
                found = contains(subroot.rKid, target);
            } else {
                found = contains(subroot.lKid, target);
            }
        }
        return found;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(E element) {
        boolean added = false;
        if(root==null) {
            root = new Node(element);
            added = true;
        } else {
            added = add(root, element);
        }
        return added;
    }

    private boolean add(Node<E> subroot, E element) {
        boolean added = false;
        if(!subroot.value.equals(element)) {
            if(subroot.value.compareTo(element)>0) {
                if(subroot.lKid==null) {
                    added = true;
                    subroot.lKid = new Node(element);
                } else {
                    added = add(subroot.lKid, element);
                }
            } else {
                if(subroot.rKid==null) {
                    added = true;
                    subroot.rKid = new Node(element);
                } else {
                    added = add(subroot.rKid, element);
                }
            }
        }
        return added;
    }


    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        root = null;
    }
}
