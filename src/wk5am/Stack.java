package wk5am;

import java.util.ArrayList;
import java.util.List;

public class Stack<E> implements PureStack<E> {
    private List<E> worker;

    public Stack() {
        worker = new ArrayList<>();
    }
    @Override
    public E peek() {
        return worker.get(worker.size()-1);
    }

    @Override
    public E pop() {
        return worker.remove(worker.size()-1);
    }

    @Override
    public boolean push(E element) {
        return worker.add(element);
    }

    @Override
    public boolean isEmpty() {
        return worker.isEmpty();
    }
}
