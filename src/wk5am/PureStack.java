package wk5am;

public interface PureStack<E> {
    E peek();

    E pop();

    boolean push(E element);

    boolean isEmpty();
}
