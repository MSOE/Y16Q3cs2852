package wk5am;

public interface PureQueue<E> {
    boolean offer(E element);
    E poll();
    E peek();
}
