package wk5am;

import java.util.LinkedList;
import java.util.List;

public class Queue<E> implements PureQueue<E> {
    private List<E> worker;
    private final int capacity;
    private static final int DEFAULT_CAPACITY = 64;

    public Queue() {
        this(DEFAULT_CAPACITY);
    }

    public Queue(int capacity) {
        worker = new LinkedList<>();
        this.capacity = capacity;
    }

    @Override
    public boolean offer(E element) {
        if(element==null) {
            throw new NullPointerException("Queue does not support null elements.");
        }
        boolean added = false;
        if(worker.size()<capacity) {
            added = worker.add(element);
        }
        return added;
    }

    @Override
    public E poll() {
        return worker.size()>0 ? worker.remove(0) : null;
    }

    @Override
    public E peek() {
        return worker.size()>0 ? worker.get(0) : null;
    }
}





