package wk5am;

public class Recursion {
    public static void main(String[] args) {
        System.out.println("Answer: " + fib(10));
    }

    private static int fib(int i) {
        System.out.println(i);
        int answer = 1;
        if(i==0) {
            answer = 0;
        } else if(i>2) {
            answer = fib(i-1) + fib(i-2);
        }
        return answer;
    }
}
