package wk2pm;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LinkedList<E> implements List<E> {

    private Node head;

    private class Node {
        private E value;
        private Node next;

        public Node(E value) {
            this(value, null);
        }

        public Node(E value, Node next) {
            this.value = value;
            this.next = next;
        }
    }
    @Override
    public boolean isEmpty() {
        return head==null;
    }

    @Override
    public void clear() {
        head = null;
    }

    @Override
    public int size() {
        int count = 0;
        Node walker = head;
        while(walker!=null) {
            ++count;
            walker = walker.next;
        }
        return count;
    }

    @Override
    public boolean add(E element) {
        if(head==null) {
            head = new Node(element);
        } else {
            Node walker = head;
            while(walker.next!=null) {
                walker = walker.next;
            }
            walker.next = new Node(element);
        }
        return true;
    }

    @Override
    public boolean remove(Object target) {
        return false;
    }

    @Override
    public E get(int index) {
        Node walker = walkTo(index);
        return walker.value;
    }

    @Override
    public E set(int index, E element) {
        Node walker = walkTo(index);
        E oldValue = walker.value;
        walker.value = element;
        return oldValue;
    }

    @Override
    public void add(int index, E element) {
        throw new IndexOutOfBoundsException();
    }

    @Override
    public E remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object target) {
        boolean found = false;
        int index = -1;
        Node walker = head;
        while(!found && walker!=null) {
            if(walker.value==null ? target==null : walker.equals(target)) {
                found = true;
            }
            walker = walker.next;
            ++index;
        }
        if(!found) {
            index = -1;
        }
        return index;
    }

    @Override
    public boolean contains(Object target) {
        return indexOf(target)!=-1;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    private void rangeCheck(int index) {
        if(index<0 || index>=size()) {
            throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
        }
    }

    private Node walkTo(int index) {
        if(index<0) {
            throw new IndexOutOfBoundsException("Index is negative: " + index);
        }
        Node walker = head;
        try {
            for (int i = 0; i < index; ++i) {
                walker = walker.next;
            }
        } catch (NullPointerException e) {
            throw new IndexOutOfBoundsException("Index is too big: " + index);
        }
        return walker;
    }

    @Override
    public Iterator<E> iterator() {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("This operation is unsupported.");
    }
}
