package wk1b;

import java.util.*;

public class ArrayList<E> implements List<E> {

    private E[] data;

    public ArrayList() {
        data = (E[])new Object[0];
    }

    @Override
    public int size() {
        return data.length;
    }

    @Override
    public boolean isEmpty() {
        return size()==0;
    }

    @Override
    public boolean contains(Object target) {
        return indexOf(target)!=-1;
    }

    @Override
    public Object[] toArray() {
        return data;
    }

    @Override
    public boolean add(E element) {
        // Make an array one bigger than before
        E[] newGuy = (E[])new Object[size()+1];
        // Copy elements from old array to new array
        for(int i=0; i<size(); i++) {
            newGuy[i] = data[i];
        }
        // Plop element into last spot in array
        newGuy[newGuy.length-1] = element;
        data = newGuy;
        return true;
    }

    @Override
    public boolean remove(Object target) {
        int position = indexOf(target);
        boolean targetFound = position!=-1;
        if(targetFound) {
            remove(position);
        }
        return targetFound;
    }

    @Override
    public void clear() {
        data = (E[])new Object[0];
    }

    @Override
    public E get(int index) {
        rangeCheck(index);
        return data[index];
    }

    @Override
    public E set(int index, E element) {
        rangeCheck(index);
        E replacedElement = get(index);
        data[index] = element;
        return replacedElement;
    }

    @Override
    public void add(int index, E element) {
        rangeCheck(index);
        E[] newGuy = (E[])new Object[size()+1];
        for(int i=0; i<index; ++i) {
            newGuy[i] = data[i];
        }
        newGuy[index] = element;
        for(int i=index; i<size(); ++i) {
            newGuy[i+1] = data[i];
        }
        data = newGuy;
    }

    @Override
    public E remove(int index) {
        E removedElement = get(index);
        E[] newGuy = (E[])new Object[size()-1];
        for(int i=0; i<index; ++i) {
            newGuy[i] = data[i];
        }
        for(int i=index; i<size()-1; ++i) {
            newGuy[i] = data[i+1];
        }
        data = newGuy;
        return removedElement;
    }

    @Override
    public int indexOf(Object target) {
        int index = 0;
        while(index<size() && target!=get(index) && (target==null || !target.equals(get(index)))) {
            ++index;
        }
        if(index==size()) {
            index = -1;
        }
        return index;
    }

    private void rangeCheck(int index) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
        }
    }

    private class ArrayListIterator implements Iterator<E> {
        private int position = -1;
        private boolean canRemove = false;

        @Override
        public boolean hasNext() {
            return position+1<size();
        }

        @Override
        public E next() {
            if(!hasNext()) {
                throw new NoSuchElementException("Nothing left for you, so sorry");
            }
            canRemove = true;
            return data[++position];
        }

        @Override
        public void remove() {
            if(!canRemove) {
                throw new IllegalStateException("Must call next() first");
            }
            canRemove = false;
            ArrayList.this.remove(position--);
        }
    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayListIterator();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Too lazy to implement.");
    }
}
