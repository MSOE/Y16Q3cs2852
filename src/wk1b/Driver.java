package wk1b;

import java.util.*;

public class Driver {
    public static void main(String[] args) {
        Collection<String> words = new ArrayList<>();
        words.add("first");
        words.add("2");
        words.add("3rd");

        {
            Iterator<String> itr = words.iterator();
            while (itr.hasNext()) {
                String word = itr.next();
                System.out.println(word);
            }
        }
        Iterator<String> itr = words.iterator();
        itr.next();
        itr.remove();

        for(String word : words) {
            System.out.println(word);
        }
    }
}






