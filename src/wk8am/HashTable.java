package wk8am;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

public class HashTable<E> implements Set<E> {
    private LinkedList<E>[] table;
    private static final int CAPACITY = 10;
    private static final double LOAD_THRESHOLD = 0.75;

    public static void main(String[] args) {
        Set<String> words = new HashTable<>();
        words.add("some");
        words.add("strings");
        words.add("are");
        words.add("added");
        words.add("now");
    }

    public HashTable() {
        table = new LinkedList[CAPACITY];
    }

    @Override
    public boolean add(E element) {
        boolean added = false;
        int index = Math.abs(element.hashCode()%table.length);
        if(table[index]==null) {
            table[index] = new LinkedList<>();
        }
        if(!table[index].contains(element)) {
            if(size()/table.length>LOAD_THRESHOLD) {
                expandTable();
                index = Math.abs(element.hashCode()%table.length);
            }
            table[index].add(element);
            added = true;
        }
        return added;
    }

    private void expandTable() {
        LinkedList<E>[] newTable = new LinkedList[2*table.length];
        for(LinkedList<E> list : table) {
            if(list!=null) {
                for(E element : list) {
                    int index = Math.abs(element.hashCode()%newTable.length);
                    if(newTable[index]==null) {
                        newTable[index] = new LinkedList<>();
                    }
                    newTable[index].add(element);
                }
            }
        }
        table = newTable;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }
}
