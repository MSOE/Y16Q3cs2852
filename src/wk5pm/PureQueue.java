package wk5pm;

public interface PureQueue<E> {
    boolean offer(E element);
    E poll();
    E peek();
    boolean isEmpty();
}
