package wk5pm;

import wk2pm.LinkedList;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;

public class Stack<E> implements PureStack<E> {
    private List<E> worker;

    public Stack() {
        worker = new LinkedList<>();
    }

    @Override
    public boolean isEmpty() {
        return worker.isEmpty();
    }

    @Override
    public void push(E element) {
        worker.add(0, element);
    }

    @Override
    public E pop() {
        if(isEmpty()) {
            throw new EmptyStackException();
        }
        return worker.remove(0);
    }

    @Override
    public E peek() {
        if(isEmpty()) {
            throw new EmptyStackException();
        }
        return worker.get(0);
    }
}
