package wk5pm;

import java.util.LinkedList;

public class Queue<E> implements PureQueue<E> {
    private LinkedList<E> worker;

    public Queue() {
        worker = new LinkedList<>();
    }

    @Override
    public boolean offer(E element) {
        if(element==null) {
            throw new NullPointerException("This Queue does not support null values");
        }
        worker.add(element);
        return true;
    }

    @Override
    public E poll() {
        return isEmpty() ? null : worker.remove(0);
    }

    @Override
    public E peek() {
        return isEmpty() ? null : worker.get(0);
    }

    @Override
    public boolean isEmpty() {
        return worker.isEmpty();
    }
}
