package wk5pm;

import java.util.Scanner;

public class SomeCode {
    public static void main(String[] args) {
        int i = 32;
        Scanner in = new Scanner(System.in);
        someMethod(i);
    }

    private static boolean someMethod(int x) {
        String useless = "Ignore me please.";
        {
            double xx = 8;
        }
        boolean returnValue = x%3==0;
        return returnValue;
    }
}
