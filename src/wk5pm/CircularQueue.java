package wk5pm;

public class CircularQueue<E> implements PureQueue<E> {
    private E[] data;
    private int head;
    private int tail;
    private boolean isEmpty;

    private static final int DEFAULT_CAPACITY = 10;

    public CircularQueue() {
        this(DEFAULT_CAPACITY);
    }

    public CircularQueue(int capacity) {
        data = (E[])new Object[capacity];
        head = 0;
        tail = 0;
        isEmpty = true;
    }

    @Override
    public boolean offer(E element) {
        if(element==null) {
            throw new NullPointerException("This CircularQueue does not support nulls.");
        }
        boolean added = false;
        if(isEmpty || head!=tail) {
            data[tail] = element;
            tail = (tail+1)%data.length;
            isEmpty = false;
            added = true;
        }
        return added;
    }

    @Override
    public E poll() {
        return null;
    }

    @Override
    public E peek() {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return isEmpty;
    }
}
