package wk5pm;

public interface PureStack<E> {
    boolean isEmpty();
    void push(E element);
    E pop();
    E peek();
}
